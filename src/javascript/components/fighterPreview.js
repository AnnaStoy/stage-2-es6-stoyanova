import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(fighter){
    fighterElement.appendChild(createImage(fighter.source));
    fighterElement.appendChild(createName(fighter.name));
    fighterElement.appendChild(createHealth(fighter.health));
    fighterElement.appendChild(createAttack(fighter.attack));
    fighterElement.appendChild(createDefense(fighter.defense));
  }

  return fighterElement;
}

function createName(name) {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = `Name: ${name}`;

  return nameElement;
}

function createHealth(health) {
  const healthElement = createElement({ tagName: 'span', className: 'health' });
  healthElement.innerText = `Health: ${health}`;

  return healthElement;
}

function createAttack(attack) {
  const attackElement = createElement({ tagName: 'span', className: 'health' });
  attackElement.innerText = `Attack: ${attack}`;

  return attackElement;
}

function createDefense(defense) {
  const defenseElement = createElement({ tagName: 'span', className: 'defense' });
  defenseElement.innerText = `Defense: ${defense}`;

  return defenseElement;
}

function createImage(source) {
  const attributes = { src: source, style: 'max-height:230px'};
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-image',
    attributes
  });

  return imgElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
