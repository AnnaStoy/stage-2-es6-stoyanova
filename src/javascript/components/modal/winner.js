import {showModal} from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const body = createElement({ tagName: 'div', className: 'body-element' });
  showModal({title: fighter.name, bodyElement: body});
}
