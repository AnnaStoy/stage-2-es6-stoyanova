import {
  controls
} from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const healthBarFirst = document.querySelector('#left-fighter-indicator');
  const healthBarSecond = document.querySelector('#right-fighter-indicator');
  let firstHealthBar = 100;
  let secondHealthBar = 100;
  const FIRST_HEALTH = firstFighter.health;
  const SECOND_HEALTH = secondFighter.health;
  // resolve the promise with the winner when fight is over
  return new Promise((resolve) => {
    const attackFirstFighter = {
      attack: false,
      block: false
    };
    const attackSecondFighter = {
      attack: false,
      block: false
    };
    document.addEventListener('keydown', function (e) {
      if (secondFighter.health > 0 && firstFighter.health > 0) {
        if (e.key == getKey(controls.PlayerOneAttack)) {
          attackFirstFighter.attack = true;
        } else if (e.key == getKey(controls.PlayerTwoAttack)) {
          attackSecondFighter.attack = true;
        } else if (e.key == getKey(controls.PlayerOneBlock)) {
          attackSecondFighter.block = true;
        } else if (e.key == getKey(controls.PlayerTwoBlock)) {
          attackFirstFighter.block = true;
        }

        if (attackFirstFighter.attack && !attackFirstFighter.block && attackSecondFighter.block == false) {
          let demage = getDamage(firstFighter, secondFighter);
          secondFighter.health -= demage;
          secondHealthBar = changeHelthBar(healthBarSecond, secondHealthBar, SECOND_HEALTH, demage);
          attackFirstFighter.attack = false;
        } else if (attackFirstFighter.attack && attackFirstFighter.block) {
          attackFirstFighter.attack = false;
          attackFirstFighter.block = false;
        }

        if (attackSecondFighter.attack && !attackSecondFighter.block && attackFirstFighter.block == false) {
          let demage = getDamage(secondFighter, firstFighter);
          firstFighter.health -= demage;
          firstHealthBar = changeHelthBar(healthBarFirst, firstHealthBar, FIRST_HEALTH, demage);
          attackSecondFighter.attack = false;
        } else if (attackSecondFighter.attack && attackSecondFighter.block) {
          attackSecondFighter.attack = false;
          attackSecondFighter.block = false;
        }
      } else {
        if (firstFighter.health <= 0) {
          resolve(secondFighter);
        } else {
          resolve(firstFighter);
        }
      }

    });
  });

}

function changeHelthBar(domElement, currentHealth, fullHealth, demage) {
  currentHealth -= demage * 100 / fullHealth;
  domElement.style.width = `${currentHealth}%`;
  return currentHealth;
}

function getKey(key) {
  return key.toLowerCase().slice(3);
}

export function getDamage(attacker, defender) {
  // return damage
  return Math.abs(getHitPower(attacker) - getBlockPower(defender));

}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = 1 + Math.random() * 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  let criticalHitChance = 1 + Math.random() * 1;
  return fighter.defense * criticalHitChance;
}
